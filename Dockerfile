# FROM $BASE_IMAGE   ### this is coming from external scripts
ARG BASE_IMAGE
FROM ${BASE_IMAGE}

ARG PYTHON_VERS

# do a yum install
RUN yum install -y automake autoconf libtool nano cppunit-devel

# inject files
COPY . /io
ADD https://boostorg.jfrog.io/artifactory/main/release/1.80.0/source/boost_1_80_0.tar.gz /io/boost.tgz

# expand boost
RUN mkdir /boost
RUN tar -xzf /io/boost.tgz -C /boost/

## run script
RUN chmod +x /io/install_boost.sh
RUN /io/install_boost.sh "${PYTHON_VERS}"

## cleanup
RUN rm -rf /io
